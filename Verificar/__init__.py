# FILTRO ANTIALIASING
def filtro_antialiasing(f_in, f_out):
    if f_out >= 2*f_in:
        return True
    else:
        return False


# VERIFICAÇÃO DE DIFERENÇA DE FREQUÊNCIA E DE TEMPO DE AMOSTRAGEM
def dif_freq_n(f_out1, f_out2, t_1, t_2):
    if f_out1 == f_out2 and t_1 == t_2:
        print(('\n\033[1;32mFrequências de Saída Iguais\n'
               'e Tempo de Amostragem Iguais.\033[m\n'))
        return True

    elif f_out1 != f_out2 and t_1 == t_2:
        print(('\n\033[1;35mFrequências de Saída Diferentes\n'
               'e Número de amostras Iguais.\033[m\n'))
        return False

    elif f_out1 == f_out2 and t_1 != t_2:
        print(('\n\033[1;35mFrequências de Saída Iguais\n'
               'e Tempo de Amostragem Diferentes.\033[m\n'))
        return False

    else:
        print(('\n\033[1;35mFrequências de Saída e\n'
               'Tempo de amostragem, ambos DIFERENTES.\033[m\n'))
        return False
