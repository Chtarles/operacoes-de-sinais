import numpy as np


def gerar_cos(A, Fin, Fs, tempo):
    W = Fin * (2 * np.pi)  # Frequencia angular
    ts = np.linspace(0, tempo, Fs)  # (inicio, fim, passo) ; Faixa de valores do tempo
    y = A * (np.cos(ts * W))  # Função Cossenoidal do sinal
    print(f'y = {y}')
    return y, ts
