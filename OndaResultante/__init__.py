import numpy as np


# VALIDAÇÃO PARA TODOS OS DADOS ESTAREM INSERIDOS
def validar_plot_resultante(y_1, y_2):
    y1 = y_1
    y2 = y_2
    if len(y1) != 0 and len(y2) != 0:
        return True
    else:
        return False


# PLOTAGEM DO SINAL RESULTANTE - SOMA
def plot_resultante(y1, y2, freq_out, tempo, amp, desl):
    # SOMA DOS SINAIS
    tamanho = int(len(y1))
    soma = []
    y = []
    for i in range(0, tamanho):
        soma.append(0)
    print(f'soma zerada = {soma}')

    for j in range(0, tamanho):
        soma[j] = y1[j] + y2[j]
    print(f'soma y = {soma}')

    # AMPLIFICAÇÃO
    if amp > 1:
        for l in range(0, tamanho):
            y.append(0)
        for k in range(0, tamanho):
            y[k] = amp * soma[k]  # Sinal Resultante
    else:
        y = soma  # Sinal Resultante

    # DESLOCAMENTO
    x = []
    indice = int(len(y2))
    if desl > 0:    # ATRASO DO SINAL
        for i in range(0, desl):
            x.insert(i, 0)

        cont = 0
        for j in range(desl, indice):
            x.insert(j, y[cont])
            cont += 1
        var_controle = indice-desl
        if cont == var_controle:
            cont = 0
        Yr = (x)

    elif desl < 0:  # ADIANTAMENTO DO SINAL
        decres = (-1)*desl
        difer = indice - decres
        cont = decres
        for j in range(0, difer):
            x.insert(j, y[cont])
            cont += 1
        var_controle = difer + decres
        if cont == var_controle:
            cont = 0

        for i in range(0, decres):
            x.append(0)
        Yr = (x)

    else:   # DESLOCAMENTO NULO
        Yr = (y)

    print(f'YR = {Yr}')
    Ts = np.linspace(0, tempo, freq_out)
    return Yr, Ts
